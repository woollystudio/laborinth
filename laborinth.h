/* CONST DECLARATIONS */

#define HOW_MANY_SENSORS 4   // x2 sensors per face
// #define HOW_MANY_SENSORS 2   // x2 sensors per face
#define HOW_MANY_TRIGGERS 2  // A-Face, B-Face
// #define HOW_MANY_TRIGGERS 1  // A-Face, B-Face
#define TIMEOUT 2500         // ms
#define START_PIN 2          // 2, 3, 4, 5, etc.


/* FOO's declarations */

void noteOn(byte channel, byte pitch, byte velocity);
void noteOff(byte channel, byte pitch, byte velocity);
void controlChange(byte channel, byte control, byte value);

typedef struct {
  uint8_t pins[HOW_MANY_SENSORS / HOW_MANY_TRIGGERS];
  bool state;
  bool lastState;
  bool player;
  long timer;
} Trigger;

Trigger triggers[HOW_MANY_TRIGGERS];