#include "MIDIUSB.h"
#include "laborinth.h"


/* SETUP */

void setup() {
  // Init USB com' port
  Serial.begin(115200);

  // Init triggers objects
  for (int i = 0; i < HOW_MANY_TRIGGERS; i++) {
    for (int j = 0; j < sizeof(triggers[i].pins); j++) {
      triggers[i].pins[j] = START_PIN + i * HOW_MANY_SENSORS / HOW_MANY_TRIGGERS + j;
      pinMode(triggers[i].pins[j], INPUT_PULLUP);
    }
    //
    triggers[i].state = false;
    triggers[i].lastState = false;
    triggers[i].player = false;
    triggers[i].timer = 0;
  }

  // On board LED
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}


/* LOOP */

void loop() {
  // Update triggers' states
  for (int i = 0; i < HOW_MANY_TRIGGERS; i++) {
    // Considering everyone is false first
    triggers[i].state = false;
    // Check for each sensor
    for (int j = 0; j < sizeof(triggers[i].pins); j++) {
      // If at least one of triggers is true, state is true
      if (digitalRead(triggers[i].pins[j]))
        triggers[i].state = true;
    }

    // If player was playing but nobody came so time is over
    if (triggers[i].player && millis() - triggers[i].timer > TIMEOUT) {
      // Send the MIDI trig
      controlChange(i, 0, 0);  // controller 0, channel 1, value 0
      MidiUSB.flush();
      // Update the timer
      triggers[i].timer = millis();
      // Update the trigger player
      triggers[i].player = false;
      // Light OFF the LED
      digitalWrite(13, LOW);
    }

    // If a trig is detected and player was not playing
    if (triggers[i].state && !triggers[i].player && millis() - triggers[i].timer > TIMEOUT) {
      // Send the MIDI trig
      controlChange(i, 0, 127);  // controller 0, channel 1, value 127
      MidiUSB.flush();
      // Update the timer
      triggers[i].timer = millis();
      // Update the trigger player
      triggers[i].player = true;
      // Light ON the LED
      digitalWrite(13, HIGH);
    }

    // If a trig is detected and player was playing
    if (triggers[i].state && triggers[i].player) {
      // Update the timer
      triggers[i].timer = millis();
    }

    // Update lastState
    triggers[i].lastState = triggers[i].state;
  }
}


/* FOO's */

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = { 0x09, 0x90 | channel, pitch, velocity };
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = { 0x08, 0x80 | channel, pitch, velocity };
  MidiUSB.sendMIDI(noteOff);
}

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = { 0x0B, 0xB0 | channel, control, value };
  MidiUSB.sendMIDI(event);
}
