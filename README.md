# laborinth

Last OS img available 
[here](https://nubo.woollystud.io/s/PCAWnyyLADemY4H). 
Build for Raspberry Pi 3B+, based on Patchbox OS.

Medias must be stored on a USB stick named "DATA" in data-A/ and data-B/ 
directories.

![gui](/media/gui.png)
![patchbox-config](/media/patchbox-config.png)
